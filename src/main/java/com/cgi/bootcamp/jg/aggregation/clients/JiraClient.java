package com.cgi.bootcamp.jg.aggregation.clients;

import com.cgi.bootcamp.jg.aggregation.api.jira.*;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class JiraClient extends WebServiceGatewaySupport {

    public TicketDTO getTicketById(Long id) {
        GetTicketByIdRequest request = new GetTicketByIdRequest();
        request.setId(id);
        GetTicketByIdResponse response = (GetTicketByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
        return response.getTicketDTO();
    }

    public TicketDTO getTicketByName(String name) {
        GetTicketByNameRequest request = new GetTicketByNameRequest();
        request.setName(name);
        GetTicketByNameResponse response = (GetTicketByNameResponse) getWebServiceTemplate().marshalSendAndReceive(request);
        return response.getTicketDTO();
    }
}

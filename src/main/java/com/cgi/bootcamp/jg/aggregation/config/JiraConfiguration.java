package com.cgi.bootcamp.jg.aggregation.config;

import com.cgi.bootcamp.jg.aggregation.clients.JiraClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class JiraConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.cgi.bootcamp.jg.aggregation.api.jira");
        return marshaller;
    }

    @Bean
    public JiraClient jiraClient(Jaxb2Marshaller marshaller) {
        JiraClient client = new JiraClient();
        client.setDefaultUri("http://localhost:8831/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}

package com.cgi.bootcamp.jg.aggregation.service;

import com.cgi.bootcamp.jg.aggregation.api.Origin;
import com.cgi.bootcamp.jg.aggregation.api.OutCompleteTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.OutDetailedTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.OutTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.jira.TicketDTO;
import com.cgi.bootcamp.jg.aggregation.api.servicenow.TicketDto;
import com.cgi.bootcamp.jg.aggregation.clients.JiraClient;
import com.cgi.bootcamp.jg.aggregation.clients.ServiceNowClient;
import com.cgi.bootcamp.jg.aggregation.clients.UserManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class TicketDetailsService {

    private JiraClient jiraClient;
    private ServiceNowClient serviceNowClient;
    private UserManagementClient userManagementClient;

    @Autowired
    public TicketDetailsService(
            JiraClient jiraClient,
            ServiceNowClient serviceNowClient,
            UserManagementClient userManagementClient) {
        this.jiraClient = jiraClient;
        this.serviceNowClient = serviceNowClient;
        this.userManagementClient = userManagementClient;
    }

    public OutTicketDto getTicket(Origin origin, Long id) {
        OutTicketDto ticket = new OutTicketDto();
        switch (origin) {
            case jira:
                ticket = mapToOutTicket(jiraClient.getTicketById(id));
                break;
            case servicenow:
                ticket = mapToOutTicket(serviceNowClient.getTicketById(id));
        }
        ticket.setOrigin(origin);
        return ticket;
    }

    public OutDetailedTicketDto getDetailedTicket(Origin origin, Long id) {
        OutDetailedTicketDto ticket = new OutDetailedTicketDto();
        Long idPersonCreator = 0L;
        Long idPersonAssigned = 0L;
        switch (origin) {
            case jira:
            {
                TicketDTO oldTicket = jiraClient.getTicketById(id);
                ticket = mapToOutDetailedTicket(oldTicket);
                idPersonCreator = oldTicket.getIdPersonCreator();
                idPersonAssigned = oldTicket.getIdPersonAssigned();
                break;
            }
            case servicenow:
            {
                TicketDto oldTicket = serviceNowClient.getTicketById(id);
                ticket = mapToOutDetailedTicket(oldTicket);
                idPersonCreator = oldTicket.getIdPersonCreator();
                idPersonAssigned = oldTicket.getIdPersonAssigned();
            }
        }
        ticket.setOrigin(origin);
        ticket.setPersonCreator(userManagementClient.getPesonSimpleById(idPersonCreator));
        ticket.setPersonAssigned(userManagementClient.getPesonSimpleById(idPersonAssigned));
        return ticket;
    }

    public OutCompleteTicketDto getCompleteTicket(Origin origin, Long id) {
        OutCompleteTicketDto ticket = new OutCompleteTicketDto();
        Long idPersonCreator = 0L;
        Long idPersonAssigned = 0L;
        switch (origin) {
            case jira:
            {
                TicketDTO oldTicket = jiraClient.getTicketById(id);
                ticket = mapToOutCompleteTicket(oldTicket);
                idPersonCreator = oldTicket.getIdPersonCreator();
                idPersonAssigned = oldTicket.getIdPersonAssigned();
                break;
            }
            case servicenow:
            {
                TicketDto oldTicket = serviceNowClient.getTicketById(id);
                ticket = mapToOutCompleteTicket(oldTicket);
                idPersonCreator = oldTicket.getIdPersonCreator();
                idPersonAssigned = oldTicket.getIdPersonAssigned();
            }
        }
        ticket.setOrigin(origin);
        ticket.setPersonCreator(userManagementClient.getPesonCompleteById(idPersonCreator));
        ticket.setPersonAssigned(userManagementClient.getPesonCompleteById(idPersonAssigned));
        return ticket;
    }

    private static LocalDateTime mapToLocalDateTime(XMLGregorianCalendar calender) {
        return calender.toGregorianCalendar().toZonedDateTime().toLocalDateTime();
    }

    private static OutCompleteTicketDto mapToOutCompleteTicket(TicketDto ticket) {
        OutCompleteTicketDto newTicket = new OutCompleteTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setCreationDatetime(LocalDateTime.parse(ticket.getCreationDatetime(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return newTicket;
    }

    private static OutCompleteTicketDto mapToOutCompleteTicket(TicketDTO ticket) {
        OutCompleteTicketDto newTicket = new OutCompleteTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setCreationDatetime(mapToLocalDateTime(ticket.getCreationDatetime()));
        newTicket.setCloseDatetime(mapToLocalDateTime(ticket.getTicketCloseDatetime()));
        return newTicket;
    }

    private static OutDetailedTicketDto mapToOutDetailedTicket(TicketDTO ticket) {
        OutDetailedTicketDto newTicket = new OutDetailedTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setCreationDatetime(mapToLocalDateTime(ticket.getCreationDatetime()));
        newTicket.setCloseDatetime(mapToLocalDateTime(ticket.getTicketCloseDatetime()));
        return newTicket;
    }

    private static OutDetailedTicketDto mapToOutDetailedTicket(TicketDto ticket) {
        OutDetailedTicketDto newTicket = new OutDetailedTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setCreationDatetime(LocalDateTime.parse(ticket.getCreationDatetime(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return newTicket;
    }

    private static OutTicketDto mapToOutTicket(TicketDTO ticket) {
        OutTicketDto newTicket = new OutTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setIdPersonCreator(ticket.getIdPersonCreator());
        newTicket.setIdPersonAssigned(ticket.getIdPersonAssigned());
        newTicket.setCreationDatetime(mapToLocalDateTime(ticket.getCreationDatetime()));
        newTicket.setCloseDatetime(mapToLocalDateTime(ticket.getTicketCloseDatetime()));
        return newTicket;
    }

    private static OutTicketDto mapToOutTicket(TicketDto ticket) {
        OutTicketDto newTicket = new OutTicketDto();
        newTicket.setId(ticket.getId());
        newTicket.setName(ticket.getName());
        newTicket.setEmail(ticket.getEmail());
        newTicket.setIdPersonCreator(ticket.getIdPersonCreator());
        newTicket.setIdPersonAssigned(ticket.getIdPersonAssigned());
        newTicket.setCreationDatetime(LocalDateTime.parse(ticket.getCreationDatetime(),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return newTicket;
    }

}

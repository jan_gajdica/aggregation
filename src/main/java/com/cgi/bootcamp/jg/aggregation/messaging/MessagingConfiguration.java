package com.cgi.bootcamp.jg.aggregation.messaging;

import com.cgi.bootcamp.jg.aggregation.api.servicenow.CreateTicketDto;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;

import javax.jms.Destination;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJms
public class MessagingConfiguration {

    @Bean
    public Destination ticketsQueue() {
        return new ActiveMQQueue("servicenow.tickets.queue");
    }

}

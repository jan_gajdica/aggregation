package com.cgi.bootcamp.jg.aggregation.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

public class OutTicketDto {

    private Origin origin;
    private Long id;
    private String name;
    private String email;
    @JsonProperty("id_person_creator")
    private Long idPersonCreator;
    @JsonProperty("id_person_assigned")
    private Long idPersonAssigned;
    @JsonProperty("creation_datetime")
    private LocalDateTime creationDatetime;
    @JsonProperty("close_datetime")
    private LocalDateTime closeDatetime;

    public OutTicketDto() {}

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public LocalDateTime getCloseDatetime() {
        return closeDatetime;
    }

    public void setCloseDatetime(LocalDateTime closeDatetime) {
        this.closeDatetime = closeDatetime;
    }

    @Override
    public String toString() {
        return "OutTicketDto{" +
                "origin=" + origin +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDatetime=" + creationDatetime +
                ", closeDatetime=" + closeDatetime +
                '}';
    }
}

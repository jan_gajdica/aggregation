package com.cgi.bootcamp.jg.aggregation.clients;

import com.cgi.bootcamp.jg.aggregation.api.usermanagement.PersonCompleteDto;
import com.cgi.bootcamp.jg.aggregation.api.usermanagement.PersonSimpleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

@Component
public class UserManagementClient {

    private RestTemplate restTemplate;
    private static final String URL = "http://localhost:8833//usermanagement/api/v1";
    private static final String USERNAME = "pepa.pepa@cgi.com";
    private static final String PASSWORD = "passwd";

    @Autowired
    public UserManagementClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public PersonSimpleDto getPesonSimpleById(Long id) {
        HttpHeaders httpHeaders = createBasicAuthHeaders(USERNAME, PASSWORD);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<PersonSimpleDto> response;
        try {
            response = restTemplate
                    .exchange(URL + "/persons/" + id.toString(),
                            HttpMethod.GET,
                            httpEntity,
                            PersonSimpleDto.class
                    );
        } catch (HttpClientErrorException exception) {
            return null;
        }
        return response.getBody();
    }

    public PersonCompleteDto getPesonCompleteById(Long id) {
        HttpHeaders httpHeaders = createBasicAuthHeaders(USERNAME, PASSWORD);
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<PersonCompleteDto> response;
        try {
            response = restTemplate
                    .exchange(URL + "/persons/" + id.toString() + "/details",
                            HttpMethod.GET,
                            httpEntity,
                            PersonCompleteDto.class
                    );
        } catch (HttpClientErrorException exception) {
            return null;
        }
        return response.getBody();
    }

    public static HttpHeaders createBasicAuthHeaders(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(
                "Authorization",
                "Basic " + Base64.getEncoder().encodeToString((username + ':' + password).getBytes()));
        return headers;
    }
}

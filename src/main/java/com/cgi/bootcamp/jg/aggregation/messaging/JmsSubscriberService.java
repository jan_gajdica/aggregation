package com.cgi.bootcamp.jg.aggregation.messaging;

import com.cgi.bootcamp.jg.aggregation.api.servicenow.CreateTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.servicenow.TicketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service
public class JmsSubscriberService {

    private JmsTemplate jms;
    private Destination destination;

    @Autowired
    public JmsSubscriberService(JmsTemplate jms, Destination destination) {
        this.jms = jms;
        this.destination = destination;
    }

    public CreateTicketDto receive() {
        return (CreateTicketDto) jms.receiveAndConvert(destination);
    }
}

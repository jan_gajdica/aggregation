package com.cgi.bootcamp.jg.aggregation.api.servicenow;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PageDto {

    private List<TicketDto> tickets;
    @JsonProperty("all_tickets")
    private Long allTickets;

    public PageDto() {}

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public Long getAllTickets() {
        return allTickets;
    }

    public void setAllTickets(Long allTickets) {
        this.allTickets = allTickets;
    }

    @Override
    public String toString() {
        return "PageDto{" +
                "tickets=" + tickets +
                ", count=" + allTickets +
                '}';
    }
}

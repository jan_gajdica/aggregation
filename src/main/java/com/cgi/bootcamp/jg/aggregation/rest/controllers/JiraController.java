package com.cgi.bootcamp.jg.aggregation.rest.controllers;

import com.cgi.bootcamp.jg.aggregation.api.jira.TicketDTO;
import com.cgi.bootcamp.jg.aggregation.clients.JiraClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "Direct to jira API")
@RestController
@RequestMapping(path = "/jira/tickets")
public class JiraController {

    private JiraClient client;

    @Autowired
    public JiraController(JiraClient client) {
        this.client = client;
    }

    @ApiOperation(value = "View ticket by id", response = TicketDTO.class)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDTO> getTicketById(@ApiParam(value = "id", required = true)
                                                   @PathVariable("id") Long id) {
        return new ResponseEntity<>(client.getTicketById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "View ticket by name", response = TicketDTO.class)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDTO> getTicketByName(@ApiParam(value = "id", required = true)
                                                     @RequestParam("name") String name) {
        return new ResponseEntity<>(client.getTicketByName(name), HttpStatus.OK);
    }
}

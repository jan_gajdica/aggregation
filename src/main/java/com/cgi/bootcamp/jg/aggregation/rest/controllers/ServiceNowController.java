package com.cgi.bootcamp.jg.aggregation.rest.controllers;

import com.cgi.bootcamp.jg.aggregation.api.servicenow.PageDto;
import com.cgi.bootcamp.jg.aggregation.api.servicenow.TicketDto;
import com.cgi.bootcamp.jg.aggregation.clients.ServiceNowClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "Direct to service now API")
@RestController
@RequestMapping(path = "/servicenow/tickets")
public class ServiceNowController {

    private ServiceNowClient client;

    @Autowired
    public ServiceNowController(ServiceNowClient client) {
        this.client = client;
    }

    @ApiOperation(value = "View ticket by id", response = TicketDto.class)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDto> getTicketById(@ApiParam(value = "id", required = true)
                                                   @PathVariable("id") Long id) {
        return new ResponseEntity<>(client.getTicketById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "View ticket by name", response = TicketDto.class)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TicketDto> getTicketByName(@ApiParam(value = "name", required = true)
                                                     @RequestParam(name = "name") String name) {
        return new ResponseEntity<>(client.getTicketByName(name), HttpStatus.OK);
    }

    @ApiOperation(value = "Page of tickets ordered by id", response = PageDto.class)
    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto> getAllTickets(@ApiParam(value = "page", defaultValue = "1")
                                                 @RequestParam(name = "page", defaultValue = "1") Long page,
                                                 @ApiParam(value = "size", defaultValue = "20")
                                                 @RequestParam(name = "size", defaultValue = "20") Long size) {
        return new ResponseEntity<>(client.getAllTickets(page, size), HttpStatus.OK);
    }
}

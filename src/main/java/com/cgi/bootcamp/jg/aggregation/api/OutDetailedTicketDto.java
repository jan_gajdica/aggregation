package com.cgi.bootcamp.jg.aggregation.api;

import com.cgi.bootcamp.jg.aggregation.api.usermanagement.PersonSimpleDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

public class OutDetailedTicketDto {

    private Origin origin;
    private Long id;
    private String name;
    private String email;
    @JsonProperty("person_creator")
    private PersonSimpleDto personCreator;
    @JsonProperty("person_assigned")
    private PersonSimpleDto personAssigned;
    @JsonProperty("creation_datetime")
    private LocalDateTime creationDatetime;
    @JsonProperty("close_datetime")
    private LocalDateTime closeDatetime;

    public OutDetailedTicketDto() {}

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PersonSimpleDto getPersonCreator() {
        return personCreator;
    }

    public void setPersonCreator(PersonSimpleDto personCreator) {
        this.personCreator = personCreator;
    }

    public PersonSimpleDto getPersonAssigned() {
        return personAssigned;
    }

    public void setPersonAssigned(PersonSimpleDto personAssigned) {
        this.personAssigned = personAssigned;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public LocalDateTime getCloseDatetime() {
        return closeDatetime;
    }

    public void setCloseDatetime(LocalDateTime closeDatetime) {
        this.closeDatetime = closeDatetime;
    }

    @Override
    public String toString() {
        return "OutDetailedTicketDto{" +
                "origin=" + origin +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", personCreator=" + personCreator +
                ", personAssigned=" + personAssigned +
                ", creationDatetime=" + creationDatetime +
                ", closeDatetime=" + closeDatetime +
                '}';
    }
}

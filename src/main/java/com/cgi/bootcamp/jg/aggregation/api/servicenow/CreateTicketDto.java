package com.cgi.bootcamp.jg.aggregation.api.servicenow;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CreateTicketDto implements Serializable {

    private String name;
    private String email;
    @JsonProperty("id_person_creator")
    private Long idPersonCreator;
    @JsonProperty("id_person_assigned")
    private Long idPersonAssigned;
    @JsonProperty("creation_datetime")
    private String creationDatetime;

    public CreateTicketDto() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @Override
    public String toString() {
        return "CreateTicketDto{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDatetime='" + creationDatetime + '\'' +
                '}';
    }
}

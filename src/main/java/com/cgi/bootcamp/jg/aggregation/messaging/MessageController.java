package com.cgi.bootcamp.jg.aggregation.messaging;

import com.cgi.bootcamp.jg.aggregation.api.Origin;
import com.cgi.bootcamp.jg.aggregation.api.OutTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.servicenow.CreateTicketDto;
import com.cgi.bootcamp.jg.aggregation.service.TicketDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "/messages")
public class MessageController {

    private JmsSubscriberService jmsSubscriberService;

    @Autowired
    public MessageController(JmsSubscriberService jmsSubscriberService) {
        this.jmsSubscriberService = jmsSubscriberService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CreateTicketDto> getMessage() {
        return new ResponseEntity<>(jmsSubscriberService.receive(), HttpStatus.OK);
    }

}

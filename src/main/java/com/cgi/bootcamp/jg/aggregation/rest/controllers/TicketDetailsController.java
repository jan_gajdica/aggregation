package com.cgi.bootcamp.jg.aggregation.rest.controllers;

import com.cgi.bootcamp.jg.aggregation.api.Origin;
import com.cgi.bootcamp.jg.aggregation.api.OutCompleteTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.OutDetailedTicketDto;
import com.cgi.bootcamp.jg.aggregation.api.OutTicketDto;
import com.cgi.bootcamp.jg.aggregation.service.TicketDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Common tickets API")
@RestController
@RequestMapping(path = "/tickets")
public class TicketDetailsController {

    private TicketDetailsService service;

    @Autowired
    public TicketDetailsController(TicketDetailsService service) {
        this.service = service;
    }

    @ApiOperation(value = "View plain ticket by id", response = OutTicketDto.class)
    @GetMapping(path = "/{origin}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OutTicketDto> getTicketById(
            @ApiParam(value = "origin", required = true)
            @PathVariable("origin") Origin origin,
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getTicket(origin, id), HttpStatus.OK);
    }

    @ApiOperation(value = "View ticket with persons info by id", response = OutDetailedTicketDto.class)
    @GetMapping(path = "/{origin}/{id}/withpersons", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OutDetailedTicketDto> getDetailedTicketById(
            @ApiParam(value = "origin", required = true)
            @PathVariable("origin") Origin origin,
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getDetailedTicket(origin, id), HttpStatus.OK);
    }

    @ApiOperation(value = "View ticket with complete persons info by id", response = OutCompleteTicketDto.class)
    @GetMapping(path = "/{origin}/{id}/withdetails", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OutCompleteTicketDto> getCompleteTicketById(
            @ApiParam(value = "origin", required = true)
            @PathVariable("origin") Origin origin,
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getCompleteTicket(origin, id), HttpStatus.OK);
    }
}

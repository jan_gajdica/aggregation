package com.cgi.bootcamp.jg.aggregation.clients;

import com.cgi.bootcamp.jg.aggregation.api.servicenow.PageDto;
import com.cgi.bootcamp.jg.aggregation.api.servicenow.TicketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class ServiceNowClient {

    private RestTemplate restTemplate;
    private static final String URL = "http://localhost:8832/servicenow/api/v1";

    @Autowired
    public ServiceNowClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public TicketDto getTicketById(Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<TicketDto> response;
        try {
            response = restTemplate
                    .exchange(URL + "/tickets/" + id.toString(),
                            HttpMethod.GET,
                            httpEntity,
                            TicketDto.class
                    );
        } catch (HttpServerErrorException exception) {
            throw new HttpClientErrorException(exception.getStatusCode(), exception.getMessage());
        }
        return response.getBody();
    }

    public TicketDto getTicketByName(String name) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<TicketDto> response;
        try {
            response = restTemplate.exchange(URL + "/tickets?name=" + name,
                    HttpMethod.GET,
                    httpEntity,
                    TicketDto.class
            );
        } catch (HttpServerErrorException exception) {
            throw new HttpClientErrorException(exception.getStatusCode(), exception.getMessage());
        }
        return response.getBody();
    }

    public PageDto getAllTickets(Long page, Long size) {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<PageDto> response;
        try {
            response = restTemplate.exchange(URL + "/tickets/all?page="
                            + page.toString()
                            + "&size=" + size.toString(),
                    HttpMethod.GET,
                    httpEntity,
                    PageDto.class
            );
        } catch (HttpServerErrorException exception) {
            throw new HttpClientErrorException(exception.getStatusCode(), exception.getMessage());
        }
        return response.getBody();
    }

}

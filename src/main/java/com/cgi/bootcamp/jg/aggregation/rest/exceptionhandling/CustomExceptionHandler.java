package com.cgi.bootcamp.jg.aggregation.rest.exceptionhandling;

import com.cgi.bootcamp.jg.aggregation.api.usermanagement.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.ws.soap.client.SoapFaultClientException;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(SoapFaultClientException.class)
    public ResponseEntity<ErrorDto> soapFaultClientException(SoapFaultClientException exception) {
        return new ResponseEntity<>(new ErrorDto(exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<ErrorDto> httpClientErrorException(HttpClientErrorException exception) {
        return new ResponseEntity<>(new ErrorDto(exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> exception(Exception exception) {
        return new ResponseEntity<>(new ErrorDto(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
